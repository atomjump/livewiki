<?php

    /*
        Installation notes:
        
        npm install wordcloud
        bower install atomjump
        
        Put this script and it's fellow files on your client website. Then configure the following
    */

    //Configurable Params
    require_once("./config.php");
                                                                    

	function file_get_contents_utf8($fn) {
		 $content = file_get_contents($fn);
	     return mb_convert_encoding($content, 'UTF-8',
		      mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
	}

	function clean_output($string) {
	  //Use for cleaning data before display
	  $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
	  return $string;
	}
	
	function selected($check_lang) {
		global $lang;
		if($check_lang == $lang) { 
			echo "selected=\"selected\""; 
		}
		return;
	}
	
	$data_path = dirname(__FILE__) . "/data/";
	if(!file_exists($data_path . "specific/words.json")) {
		mkdir($data_path . "specific");
		//Copy across the original words file
		if(!copy($data_path . ".htaccess", $data_path . "specific/.htaccess")) {
			//Error copying
			echo "Error: could not copy the original .htaccess file to data/specific/.htaccess. Check your permissions.";
			exit(0);
		}
		if(!copy($data_path . "wordsORIGINAL.json", $data_path . "specific/words.json")) {
			//Error copying
			echo "Error: could not copy the original data/wordsORIGNAL.json file to data/specific/words.json. Check your permissions.";
			exit(0);
		}
	}


	
	if(!isset($msg)) {
     //Get global language file - but only once
     $data = file_get_contents_utf8($data_path ."messages.json");
     if($data) {
        $msg = json_decode($data, true);
        if(!isset($msg)) {
          echo "Error: config/messages.json is not valid JSON ";
          
          switch(json_last_error()) {
          
                case JSON_ERROR_NONE:
                    echo ' - No errors';
                break;
                case JSON_ERROR_DEPTH:
                    echo ' - Maximum stack depth exceeded';
                break;
                case JSON_ERROR_STATE_MISMATCH:
                    echo ' - Underflow or the modes mismatch';
                break;
                case JSON_ERROR_CTRL_CHAR:
                    echo ' - Unexpected control character found';
                break;
                case JSON_ERROR_SYNTAX:
                    echo ' - Syntax error, malformed JSON';
                break;
                case JSON_ERROR_UTF8:
                    echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
                default:
                    echo ' - Unknown error';
                break;
          
          }
          exit(0);
        }
        
       
        
     
     } else {
       echo "Error: Missing messages/messages.json.";
       exit(0);
     
     } 
  }
	
	//Set default language, unless otherwise set
	$lang = $msg['defaultLanguage'];
	if(isset($_COOKIE['lang'])) {
		$lang = $_COOKIE['lang'];
	}
	if(isset($_REQUEST['lang'])) {
		$lang = $_REQUEST['lang'];		//This will override the existing
	}
	

	//Ensure no caching
	header("Cache-Control: no-store, no-cache, must-revalidate, private, no-transform"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Pragma: no-cache"); // HTTP/1.0
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
    <script src="<?php echo $wordcloud_js_path ?>"></script>
    <script src="<?php echo $jquery_js_path ?>"></script>
   
	<!-- Must be before the other css files for global reach -->
    <style>
    
        html, body {
            height:100%;
            margin:0;
            padding:0px !important;
            width: 100%;
        }
 
        .container-fluid {
          height:100%;
          display:table;
          width: 100%;
          padding: 0;
          background-color: <?php echo $background_color ?>;
        }
         
        .row-fluid {
            height: 100%;
            display:table-cell;
            vertical-align: middle;
        }

        .centering {
          float:none;
          margin:0 auto;
        }
        
 
   
    </style>


    <!-- AtomJump Feedback Starts -->
   <!-- Bootstrap core CSS. Ver 3.3.1 sits in css/bootstrap.min.css -->
	  <link rel="StyleSheet" href="<?php echo $bootstrap_css_path ?>" rel="stylesheet">
	
	<!-- AtomJump Feedback CSS -->
	<link rel="StyleSheet" href="<?php echo $atomjump_css_path ?>">




	
	<!-- Bootstrap HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="<?php echo $html5shiv_path ?>"></script>
	  <script src="<?php echo $respond_path ?>"></script>
	  <script src="<?php echo $ie9_version ?>"></script>
	<![endif]-->
	
	
	<!-- Must be after the others to stress the importance -->
	<style>

   	   /* a {
			font-family: <?php echo $font_family ?> !important;
		}*/

		a.list:hover, a.list:visited, a.list:link, a.list:active
		{
			font-family: <?php echo $font_family ?> !important;
			text-decoration: none;
		}
		
		.input-group-addon {
 		    width:30%;
    		text-align:left;
		}
		
		.share-button {
			margin: 10px;
		
		}
		
		.top-group {
			margin-top: 10px;
			float: right;
		}
		
		
	 </style>


	
	<script>
				
		function myTrim(x)
		{
			return x.replace(/^\s+|\s+$/gm,'');
		}

		function getCookie(cname)
		{	
			if(document && document.cookie) {
				var name = cname + "=";
				var ca = document.cookie.split(';');
				for(var i=0; i<ca.length; i++)
				{
					var c = myTrim(decodeURIComponent(ca[i]));// ie8 didn't support .trim();
					if (c.indexOf(name)==0) return c.substring(name.length,c.length);
				}
			}
			return "";
		}
		
		function cookieOffset()
		{
		  //Should output: Thu,31-Dec-2020 00:00:00 GMT
		  var cdate = new Date;
		  var expirydate=new Date();
		  expirydate.setTime(expirydate.getTime()+(365*3*60*60*24*1000));	//3 years, although 2 years may be the limit on some browsers
		  var write = expirydate.toGMTString();
		  
		  return write;
		}
		
		function localStorageSetItem(mykey, value) {
										
			//Now set the cookie locally
			document.cookie = mykey + '=' + encodeURIComponent(value) + '; path=/; SameSite=Strict; expires=' + cookieOffset() + ';';		//Note: strict means cookies only used by this site
								
			var data = {};	
			data.name = mykey;
			data.value = value;
		
			
			//And start a server-side setting of the cookie via an http response. Important for iPhones.
			if(window.jQuery) {			//Note: the first time we run this, to do the checking of whether the cookies are working,
												//jQuery won't have been defined. We can skip the secondary setting of the cookies in this case, anyway.
				
				jQuery.ajax({
						url: "set-cookie.php",
						data: data,
						method:"POST",
						cache:false,
						crossDomain: false,
						global: false,
						error: function(err) {
							console.error(err);
						},
						success: function(data) {
							console.log(data);
						},
						complete:function(){
							console.log("Request finished.");
						}
				});
			}
			
		}
		
		var translateTable = {
				"msg": {
					"en": "Join me to chat at:",
					"ch": "加入我的聊天室：",
					"cht": "加入我的聊天室：",
					"ar": "انضم إلي للدردشة على:",
					"bg": "চ্যাট করতে আমার সাথে যোগ দিন:",
					"de": "Begleiten Sie mich zum Chatten unter:",
					"es" : "Únete a mí para charlar en:",
					"pt": "Junte-se a mim para bater um papo em:",
					"fr": "Rejoignez-moi pour discuter à:",
					"hi": "चैट करने के लिए मुझसे जुड़ें:",
					"in": "Bergabunglah dengan saya untuk mengobrol di:",
					"it": "Unisciti a me per chattare su:",
					"jp": "私と一緒にチャットしてください：",
					"ko": "다음에서 채팅에 참여하십시오.",
					"pu": "ਮੇਰੇ ਨਾਲ ਗੱਲਬਾਤ ਕਰਨ ਲਈ ਸ਼ਾਮਲ ਹੋਵੋ:",
					"ru": "Присоединяйтесь ко мне в чате:"
				},
				"msgSentence": {
					"en": "Join me to chat at ",
					"ch": "加入我的聊天室",
					"cht": "加入我的聊天室",
					"ar": "انضم إلي للدردشة على ",
					"bg": "চ্যাট করতে আমার সাথে যোগ দিন ",
					"de": "Begleiten Sie mich zum Chatten unter ",
					"es" : "Únete a mí para charlar en ",
					"pt": "Junte-se a mim para bater um papo em ",
					"fr": "Rejoignez-moi pour discuter à ",
					"hi": "चैट करने के लिए मुझसे जुड़ें ",
					"in": "Bergabunglah dengan saya untuk mengobrol di ",
					"it": "Unisciti a me per chattare su ",
					"jp": "私と一緒にチャットしてください ",
					"ko": "다음에서 채팅에 참여하십시오. ",
					"pu": "ਮੇਰੇ ਨਾਲ ਗੱਲਬਾਤ ਕਰਨ ਲਈ ਸ਼ਾਮਲ ਹੋਵ ",
					"ru": "Присоединяйтесь ко мне в чате "
				},
				"title": {
						"en": "AtomJump Messaging",
						"ch": "AtomJump 讯息传递",
						"cht": "AtomJump 訊息傳遞",
						"ar": "AtomJump المراسلة",
						"bg": "AtomJump মেসেজিং",
						"de": "AtomJump Messaging",
						"es" : "AtomJump Mensajería",
						"pt" : "AtomJump Mensagens",
						"fr": "AtomJump Messagerie",
						"hi": "AtomJump संदेश सेवा",
						"in": "AtomJump Olahpesan",
						"it": "AtomJump Messaggistica",
						"jp": "AtomJump メッセージング",
						"ko": "AtomJump 메시징",
						"pu": "AtomJump ਮੈਸੇਜਿੰਗੋ",
						"ru": "AtomJump обмен сообщениями"
				},
				"copyPaste": {
						"en": "Copy and Paste:",
						"ch": "复制和粘贴：",
						"cht": "複製和粘貼：",
						"ar": "نسخ و لصق:",
						"bg": "কপি এবং পেস্ট:",
						"de": "Kopieren und Einfügen:",
						"es" : "Copiar y pegar:",
						"pt" : "Copiar e colar:",
						"fr": "Copier et coller:",
						"hi": "प्रतिलिपि करें और चिपकाएं:",
						"in": "Salin dan tempel:",
						"it": "Copia e incolla:",
						"jp": "コピーアンドペースト：",
						"ko": "복사 및 붙여 넣기:",
						"pu": "ਕਾੱਪੀ ਅਤੇ ਪੇਸਟ ਕਰੋ:",
						"ru": "Скопировать и вставить:"
				}
				
			};
	
	
	
		//Add your configuration here for AtomJump Feedback
		var ajFeedback = {
			"uniqueFeedbackId" : "<?php echo $unique_key ?>home",		//This can be anything globally unique to your company/page	
			"myMachineUser" : "<?php echo $my_machine_user ?>",			/* Obtain this value from 1. Settings
																			2. Entering an email/Password
																			3. Click save
																			4. Settings
																			5. Clicking: 'Your password', then 'Developer Tools'
																			6. Copy the myMachineUser into here.
					
													*/
			"server":  "<?php echo $server ?>",
			"cssFeedback" : "<?php echo $atomjump_css_path ?>",
			"cssBootstrap": "<?php echo $bootstrap_css_path ?>",
			"domain": "<?php echo $same_domain ?>"
		}
		
		var global = {};
		global.defaultLang = "en";
		global.requestLang = "<?php if(isset($_REQUEST['lang'])) {
								 echo clean_output($_REQUEST['lang']);
							  } else { 
							  	echo ""; 
							  } ?>";
		global.lang = "en";
		var lang = getCookie("lang");
		
	
		   
		if(lang) {
			//Cookie exists
			
			//Check if we have a request override
			if((global.requestLang)&&(global.requestLang != lang)) {
				lang = global.requestLang;
				localStorageSetItem('lang', lang);
			}
			
			if(translateTable.title[lang]) {
				//All good	
			} else {
				if(global.defaultLang) {	
					lang = global.defaultLang;	//Default
				} else {
					lang = "en";
				}
			}
		} else { 
			
			//Cookie doesn't exist
			if((global.requestLang)&&(translateTable.title[global.requestLang])) {
				//Use a global language default - could be passed in from the 'lang' param
				
				lang = global.requestLang;
				localStorageSetItem('lang', lang);
			} else {
				if(global.defaultLang) {	
					lang = global.defaultLang;	//Default
				} else {
					lang = "en";
				}
			}
		}		
		
		

		
		
		function shareMe(alteredURL) {
  

		  
  	
  			var myURL = alteredURL.split('?')[0];		//Remove query params
			if(lang) {	
				//Keep existing
			} else {				
				var lang = getCookie("lang");
		   
				if(lang) {
					//Cookie exists
					if(translateTable.title[lang]) {
						//All good	
					} else {
						if(global.defaultLang) {	
							lang = global.defaultLang;	//Default
						} else {
							lang = "en";
						}
					}
				} else { 
					//Cookie doesn't exist
					if((global.lang)&&(translateTable.title[global.lang])) {
						//Use a global language default
						lang = global.lang;
					} else {
						if(global.defaultLang) {	
							lang = global.defaultLang;	//Default
						} else {
							lang = "en";
						}
					}
				}		
			}
			
			//Share a link in the same language
			if(lang != global.defaultLang) {
				if(myURL.slice(-1) != "/") {
					myURL = myURL + "/";
				}

				myURL = myURL + "?lang=" + lang;	
			}
			
			var myLink = "<a href='" + myURL + "' style='font-size: 150%;'><img src='images/link.svg' width='26' height='26'></a>&nbsp;&nbsp;";
	
		   isMac = false;
		   isiOSiPadOS = false;	
		   if(navigator.platform) {
				var isMac = navigator.platform.toUpperCase().indexOf('MAC')>=0;
				var isiOSiPadOS = navigator.platform.startsWith("iP") ||
		    navigator.platform.startsWith("Mac") && navigator.maxTouchPoints > 4;
			}		
			
			
		   if (navigator.share && isMac == false && isiOSiPadOS == false) {
			  navigator.share({
				  title: translateTable.title[lang],  //E.g. 'AtomJump Messaging',
				  text: translateTable.msg[lang], //E.g. 'Join me to chat at:',
				  url: myURL,
			  }).then(function() {
						console.log('Successful share'); 
						return false;
					}).catch(function(error) {
						console.log('Error sharing', error);
						
						//Share not supported - likely a desktop or iPhone - try SMS. Open up a box with some text to copy
						var myMessage = myLink + translateTable.copyPaste[lang] + "  <b>" + translateTable.msgSentence[lang] + myURL + "</b>";
						jQuery("#message").html(myMessage);
						jQuery("#message").slideToggle();
						return false;
					});
			} else {
				
			  	//Share not supported - likely a desktop or iPhone - try SMS. Open up a box with some text to copy
				var myMessage = myLink + translateTable.copyPaste[lang] + "  <b>" + translateTable.msgSentence[lang] + myURL + "</b>";
				jQuery("#message").html(myMessage);
				jQuery("#message").slideToggle();
				
			 	return true; 
			}
			 
			
		  }
  
		 
		var originalURL = window.location.href;
		
	</script>
	<script type="text/javascript" src="<?php echo $atomjump_js_path ?>"></script>
	<!-- AtomJump Feedback Ends -->
   
</head>
<body>
    <div id="comment-holder"></div><!-- holds the popup comments. Can be anywhere between the <body> tags -->
    <div class="container-fluid" >
        
        
        <div class="row">
        	<div class="col-xs-12 col-md-12">
					<div style="display: none; overflow-wrap: break-word; word-break: break-word; word-wrap: break-word;
 white-space: normal; margin-top: 10px;" class="alert alert-info alert-dismissable nowrap" id="message"></div>
			</div>
        
         	<div id="top" class="top-group">
         	
         			<span>
         				<img style="text-align: right;" src="images/flags-trans.png" width="48" height="14">
         			</span>
		     		<span id="userguidedropdown">
		     			
		     			<select id="languages" name="languages" onchange="window.location.href = originalURL.split('?')[0] + '?lang=' + this.value;">
							<option <?php selected('en'); ?> value="en">English</option>
							<option <?php selected('de'); ?> value="de">Deutsche</option>
							<option <?php selected('fr'); ?> value="fr">Français</option>
							<option <?php selected('jp'); ?> value="jp">日本語</option>
							<option <?php selected('ch'); ?> value="ch">简体中文</option>
							<option <?php selected('cht'); ?> value="cht">中國傳統的</option>
							<option <?php selected('it'); ?> value="it">Italiano</option>
							<option <?php selected('bg'); ?> value="bg">বাংলা</option>
							<option <?php selected('ko'); ?> value="ko">한국어</option>
							<option <?php selected('ru'); ?> value="ru">русский</option>
							<option <?php selected('es'); ?> value="es">Español</option>
							<option <?php selected('pt'); ?> value="pt">Português</option>
							<option <?php selected('hi'); ?> value="hi">हिंदी</option>
							<option <?php selected('pu'); ?> value="pu">ਪੰਜਾਬੀ</option>
							<option <?php selected('in'); ?> value="in">Bahasa Indonesia</option>
						</select>
					</span>
					<!--<p style="text-align: right; float: right;"></p>-->
						 	
         	
        			<span class="share-button" title="Share with a colleague">
						<a onclick="return shareMe(originalURL);" href="javascript:" id="start-share"><img width="32" src="images/share.svg"></a>
					</span>
			</div>
			
			
        
        </div>
        
        <div id="show-word-cloud" class="row" style="padding-top: 10px; width: 100%; background-color: <?php echo $background_color ?>">
            
            <div class="col-md-12">
                <div class="centering text-center">
                 
                   <span id="my-comments" class="comment-open" style="display: none;" href="javascript:">Click me for comments</span>
		            <!-- Any link on the page can have the 'comment-open' class added and a blank 'href="javascript:"' -->
	            
                   
					<div style="position: relative; width:100%; height: 768px; margin-left: auto; margin-right:auto">
						<div id="my_canvas" style="width:100%; height: 768px; "></div>
					</div>
                    
                 </div>
            </div>
            
           
            
        </div> <!-- end of row -->
 
 
        <div id="show-mobile-display" class="row" style="padding-top: 10px;">
            
            <div class="col-md-12">
                <div class="centering text-center">
                    <div id="mobile-display" style="background-color: <?php echo $background_color ?>; padding: 10px;"></div>
                 </div>
            </div>
            
        </div> <!-- end of row -->
        
 
        
        <div class="row" style="padding-top: 10px;">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="centering text-center">
                <form action="add-forum.php" class="form-inline" style="vertical-align: middle" >
                  <div class="form-group" >
                    <label class="sr-only" for="forum">Add</label>
                    <div class="input-group" style="margin: 4px;">
                        
                        <input name="new-forum" type="text" class="form-control" id="forum" placeholder="<?php echo $msg['msgs'][$lang]['newForum']; ?>">
                        <div class="input-group-addon">
                            <select name="temperature" class="form-control" style="height:20px; padding:0px;">
                              <option value="0"><?php echo $msg['msgs'][$lang]['removeMe']; ?></option>
                              <option value="3"><?php echo $msg['msgs'][$lang]['coldOpt']; ?></option>
                              <option value="4"><?php echo $msg['msgs'][$lang]['warmOpt']; ?></option>
                              <option value="5" selected="selected"><?php echo $msg['msgs'][$lang]['mildOpt']; ?></option>
                              <option value="6"><?php echo $msg['msgs'][$lang]['hotOpt']; ?></option>
                              <option value="8"><?php echo $msg['msgs'][$lang]['boilingOpt']; ?></option>
                              <option value="10"><?php echo $msg['msgs'][$lang]['setTitle']; ?></option>
                            </select>
                        </div>
                    </div> <!-- end of input group -->
                    <button id="submit-button" type="submit" class="btn btn-primary"><?php echo $msg['msgs'][$lang]['submitButton']; ?></button>
                  </div>  <!-- end of form group -->
		  <br/><br/><br/><br/><br/>
                  
                </form>
             </div>
          
           </div> <!-- end of col -->
           <div class="col-md-2"></div>            
          
        </div> <!-- end of row -->
        
 
        
    </div>

    
    
 
   
     <script>
     	
       //Word tree:
        var treeData;
        var words = {};
			
		//Courtesy detectmobilebrowsers.com
        window.mobilecheck = function() {
		  var check = false;
		  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		  return check;
		}
			
		if(window.mobilecheck() == true) {
			//Mobile version
			$('#show-word-cloud').hide();
			$('#forum').css('width', '300px;');
			
		} else {
			$('#show-mobile-display').hide();
			
		}


        var oReq = new XMLHttpRequest();
        oReq.onload = reqListener;
        oReq.open("get", "<?php echo $host ?>/data/specific/words.json?cs=" + Math.random(), true);
        oReq.setRequestHeader("Cache-Control", "no-cache");
        oReq.send();

		function getColor(word, weight) {
			var col;
					if (weight === 10) {
						col = '#007dc5';
					} else {
						var char = Math.abs(parseInt((word.charCodeAt(0) - 97)/5)); // = 0-25 /5 = 0-5
					
						var colours = [ '#AAA', '#BBB', '#CCC', '#DDD', '#FFF' ];
						if(colours[char]) {
							col = colours[char];
						} else {
							col = '#FFF';
						}
					}
					return col;
		}
		
		function clickEntry(index, size) {
		  	//Modify the local clickable feedback link, then click it
		  	var forumTitle = index[0].replace(" ", "-");		//Replace spaces with hyphens
	        $('#my-comments').data('uniquefeedbackid', "<?php echo $unique_key ?>" + forumTitle);
	        $('#my-comments').trigger("click");
	        return false;
		
		}
		

        function reqListener(e) {
            
           
            words = JSON.parse(this.responseText);
            
            //A mobile version is built for the mobile screens - a list of the text down the screen, only.
            var all= "";
            var list = words.list;
            
		    for(var cnt=0; cnt<list.length; cnt++) {
		    	var word = list[cnt][0];
		    	var weight = list[cnt][1];
		    	var col = getColor(word, weight);
		    	
		    	var fontSize = weight * 10;
		    	all = all + "<a href='javascript:' onclick='return clickEntry(words.list[" + cnt + "], null);' class='list' style='color: " + col + "; font-size:" + fontSize + "px';>" + word + "</a></br>";
		    	
		    }   
		    
		    $('#mobile-display').html(all);         
            	
            
            //Desktop version included on the same screen
	            var cloudOpts = { 
	              list: words.list,
	              gridSize: Math.round(16 * $('#my_canvas').width() / 1024),
	              weightFactor: function (size) {
	                return Math.pow(size, 2.3) * $('#my_canvas').width() / 1024;
	              },
	              fontFamily: '<?php echo $font_family ?>',
	              color: getColor,
	              rotateRatio: 0,
	              backgroundColor: '<?php echo $background_color ?>',
	              classes: 'comment-open',
	              click: clickEntry,
	              hover: function(word) {
	              },
	              shuffle: false
	              
	            }
	                                
	            WordCloud(document.getElementById('my_canvas'), cloudOpts);
           
        }
       

       
        
    </script>
    
  
</body>

</html>
